package com.smvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class FormController {
   @RequestMapping("/submit")
   public String display(HttpServletRequest req, Model m)
   {
       //read the provided form data
       String name=req.getParameter("name");
       String pass=req.getParameter("pass");
       if(pass.equals("texas"))
       {
           String msg="Hello "+ name;
           //add a message to the model
           m.addAttribute("message", msg);
           return "success";
       }
       else
       {
           String msg="Sorry "+ name+". You entered an incorrect password";
           m.addAttribute("message", msg);
           return "error";
       }
   }
}
