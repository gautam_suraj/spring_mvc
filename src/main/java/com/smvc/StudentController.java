package com.smvc;

import com.model.Student;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/student")
@Controller
public class StudentController {

    @RequestMapping("/registerStudent")
    public String register(Model model){
        Student user = new Student();
        model.addAttribute("user",user);
        return "student-registration";
    }

    @RequestMapping("/submitForm")
// @ModelAttribute binds form data to the object
    public String submitForm(@ModelAttribute("user") Student user)
    {
        return "student-conformation";
    }

}
