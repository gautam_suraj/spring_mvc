package com.smvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SmallController {
    @RequestMapping("/abc")
    public String display(){
         return "index";
    }

    @RequestMapping("/second")
    public String displaySecond(){
        return "second";
    }
}
