package com.model;

public class Student {
    private String firstName;
    private String lastName;
    private String gender;
    private String[] subjects;
    private String fromClassAddress;
    private String toClassAddress;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String[] getSubjects() {
        return subjects;
    }

    public void setSubjects(String[] subjects) {
        this.subjects = subjects;
    }

    public String getFromClassAddress() {
        return fromClassAddress;
    }

    public void setFromClassAddress(String fromClassAddress) {
        this.fromClassAddress = fromClassAddress;
    }

    public String getToClassAddress() {
        return toClassAddress;
    }

    public void setToClassAddress(String toClassAddress) {
        this.toClassAddress = toClassAddress;
    }
}
