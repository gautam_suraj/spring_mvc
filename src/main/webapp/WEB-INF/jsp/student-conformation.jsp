<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<body>
<p>Your registration is confirmed successfully. Please, re-check the details.</p>
First Name : ${user.firstName} <br>
Last Name : ${user.lastName} <br>
Gender: ${user.gender}<br>
Subjects:
<ul>
    <c:forEach var="subject" items="${user.subjects}">
        <li>${subject}</li>
    </c:forEach>
</ul>
Class From : ${user.fromClassAddress} <br>
Class On : ${user.toClassAddress}
</body>
</html>