<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<h3>User Registration Form</h3>
<body>
<form:form action="submitForm" modelAttribute="user">
    First name: <form:input path="firstName" />
    <br><br>
    Last name: <form:input path="lastName" />
    <br><br>
    Gender:
    Male <form:radiobutton path="gender" value="Male"/>
    Female <form:radiobutton path="gender" value="Female"/>
    <br><br>
    Subjects:
    Java<form:checkbox path="subjects" value="Java"/>
    Python<form:checkbox path="subjects" value="Python"/>
    PHP<form:checkbox path="subjects" value="PHP"/>
    Node<form:checkbox path="subjects" value="Node"/>
    <br><br>
    Class From: <form:select path="fromClassAddress">
    <form:option value="second" label="Second Semester"/>
    <form:option value="fifth" label="Fifth Semester"/>

</form:select>
    <br><br>
    Class On: <form:select path="toClassAddress">
    <form:option value="second" label="Second Semester Room"/>
    <form:option value="fifth" label="Fifth Semester Room"/>
</form:select>
    <br><br>
    <input type="submit" value="Submit" />
</form:form>
</body>
</html>